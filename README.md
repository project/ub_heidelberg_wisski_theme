# WissKI Theme

This theme by UB Heidelberg provides several enhancements in the presentation
of WissKI entities and their forms. With a custom CSS it can also easily be
modified for other styles and designs.

### Installing

Put the module folder into /themes and activate it under /admin/design.

### Further instructions

In addition to this theme there is a module for additional content types,
which provide blocks for the frontpage. However, it is not required to use it.
