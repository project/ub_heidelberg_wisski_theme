(function($){
$( document ).ready(function() {
	//Menu functions
        $( ".menu-item--expanded" ).addClass( "menu-item--collapsed" );
        $( ".menu-item--expanded" ).removeClass( "menu-item--expanded" );
	$( ".has-submenu" ).mouseenter(function() {
 	   $(this).addClass( "menu-item--expanded" );
           $(this).removeClass( "menu-item--collapsed" );
	});
        $( ".has-submenu" ).mouseleave(function() {
           $(this).addClass( "menu-item--collapsed" );
           $(this).removeClass( "menu-item--expanded" );
        });
        $( ".has-submenu > .menu" ).mouseenter(function() {
           $(this).parent().addClass( "menu-item--expanded" );
           $(this).parent().removeClass( "menu-item--collapsed" );
        });
        $( ".has-submenu" ).mouseleave(function() {
           $(this).parent().addClass( "menu-item--collapsed" );
           $(this).parent().removeClass( "menu-item--expanded" );
        });

	var item = $('<span class="collapse-menu" />');
        item.click(function() { 
		if($(this).parent('.has-submenu').hasClass( "menu-item--collapsed-static" )) {
 		    $(this).parent('.has-submenu').addClass( "menu-item--expanded-static" ).removeClass( "menu-item--collapsed-static" );
		}
		else if($(this).parent('.has-submenu').hasClass( "menu-item--expanded-static" )) {
		    $(this).parent('.has-submenu').addClass( "menu-item--collapsed-static" ).removeClass( "menu-item--expanded-static" );
		}
		else {
		    $(this).parent('.has-submenu').addClass( "menu-item--expanded-static" ).removeClass( "menu-item--collapsed-static" );
                }
	});
        $('.has-submenu').prepend(item);

	//Add an optional line break to WissKI labels after a "/"
	$('.field__label').html(function(){
            return this.innerHTML.replaceAll("/","/" + '<wbr>');
        });

	//Add classes to WissKI fields
	$( ".field__item" ).siblings( ".field__item" ).addClass( "wisski-theme-repeated-field" );

	$( ".field" ).parent( ".field__item" ).parent( ".field__items" ).siblings( ".field__label" ).addClass( "wisski-theme-group-label" );

	$( ".field" ).parent( ".field__item" ).parent( ".field__items" ).addClass( "wisski-theme-group-items" );

	$( ".field" ).parent( ".field__item" ).parent( ".field__items" ).parent( ".field" ).addClass( "wisski-theme-group" );

	$( ".field" ).parent( ".field__item" ).parent( ".field" ).addClass( "wisski-theme-group" );

	$( ".leaflet-container").parent( ".field__item" ).addClass( "leaflet-item" );
});
})(jQuery);

//Menu function
function toggleMenu(toggle_id) {
  var x = document.getElementById("responsive_menu");
  var y = document.getElementById(toggle_id);
  if (x.style.display === "block") {
    x.style.display = "none";
    y.classList.remove("toggle_button_expanded");
    x.classList.remove("responsive_menu_expanded");
  } else {
    x.style.display = "block";
    y.classList.add("toggle_button_expanded");
    x.classList.add("responsive_menu_expanded");
  }
}

